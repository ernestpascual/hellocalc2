import React from 'react';
import styled from 'tachyons-components'

const ButtonGroup = styled('div')`
br1 black `

const Operator = styled('button')`br1 bg-light-purple lightest-blue`

const NumberButtons = (props) => 
    (

        <ButtonGroup>
        <Operator value="add" onClick={props.opButton}> + </Operator>
        <Operator value="minus" onClick={props.opButton}> - </Operator>
        <Operator value="div" onClick={props.opButton}> / </Operator>
        <Operator value="mult" onClick={props.opButton}> x </Operator>
        <Operator value="calc" onClick={props.opButton} > = </Operator>
        <Operator onClick={props.clearButton}> CLEAR </Operator>
        <Operator onClick={props.bkButton}> DEL </Operator>
        </ButtonGroup>
  );

export default NumberButtons;
