import React from 'react';
import styled from 'tachyons-components'

const ButtonGroup = styled('div')`
br1 black`

const Number = styled('button')`br1 dim bg-light-purple lightest-blue`

const NumberButtons = (props) => 
    (

        <ButtonGroup>
        <Number value="0" onClick={props.numberButton} > 0 </Number>
        <Number value="1" onClick={props.numberButton} > 1 </Number>
        <Number value="2" onClick={props.numberButton} > 2 </Number>
        <Number value="3" onClick={props.numberButton} > 3 </Number>
        <Number value="4" onClick={props.numberButton} > 4 </Number>
        <Number value="5" onClick={props.numberButton} > 5 </Number>
        <Number value="6" onClick={props.numberButton} > 6 </Number>
        <Number value="7" onClick={props.numberButton} > 7 </Number>
        <Number value="8" onClick={props.numberButton} > 8 </Number>
        <Number value="9" onClick={props.numberButton} > 9 </Number>
        </ButtonGroup>
  );

export default NumberButtons;
