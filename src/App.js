import React, {
  Component
} from 'react';
import './App.css';
import NumberButtons from './components/NumberButtons'
import OperationButtons from './components/OperationButtons'
import styled from 'tachyons-components'

var screenPadding = { "border-style":"solid", "width" : "20vw", "height": "30px", "margin-left": "1vw"};
var buttonkembot = {"margin-left" : "1vw","margin-right" : "2vw", "text-align" : "center"}
var Calcuer = {"width": "300px", "height": "250px", "left": "18vw", "position" : "relative", "zoom" : "1.8", "text-align" : "center"}
var kemerloo = {"padding-top" : "4vh"}

var Lalagyan = styled('div')`
bg-hot-pink yellow
`


export default class App extends Component {
  state = {
    hello: "MAMA MO! CALCU",
    screen: "",
    operation: "",
    prev:""
  }

  numberButton = (event) => {
    // number pressing

    if (event.target.value === "0"){
      if(this.state.screen.length < 1){
        alert("Please enter a non-zero number")
      } else {
        this.setState({
          screen: this.state.screen + event.target.value,
        })
      }
    }else {
      if(this.state.screen.length <= 12){
        this.setState({
          screen: this.state.screen + event.target.value,
        })
      } else {
        alert('12 charachters lang mumsh!')
      }

    }
  }


  clearButton = (event) => {
    // Clear button
    this.setState({
      screen: ""
    })
  }


  opButton = (event) => {
    // perform calculation
    if(event.target.value === "calc"){
      if(this.state.operation === "add"){
      this.setState({
        screen: parseInt(this.state.prev) + parseInt(this.state.screen),
        operation: "",
        prev: ""
      })
     } 
     else if(this.state.operation === "minus") {
      this.setState({
        screen: parseInt(this.state.prev) - parseInt(this.state.screen),
        operation: "",
        prev: ""
      })
     }
     else if(this.state.operation === "div") {
      this.setState({
        screen: parseInt(this.state.prev) / parseInt(this.state.screen),
        operation: "",
        prev: ""
      })
     }

     else if(this.state.operation === "mult") {
      this.setState({
        screen: parseInt(this.state.prev) * parseInt(this.state.screen),
        operation: "",
        prev: ""
      })
     }
    } else {
      // Set operation
      this.setState({
        prev: this.state.screen,
        operation: event.target.value,
        screen: ""
      })
    }
  }

  bkButton = event => {
    this.setState({screen: this.state.screen.toString().slice(0, -1)})
  }

  render() {
    return ( 
    <Lalagyan style={Calcuer} onKeyPress={this.numberButton}>
      <h1 style={kemerloo} >{this.state.hello} </h1>
      <h3 style={screenPadding}>{this.state.screen} </h3>
      <div style = {buttonkembot} >
      <NumberButtons numberButton={this.numberButton} />
       <OperationButtons opButton={this.opButton} clearButton={this.clearButton} bkButton={this.bkButton} />
       </div>
    </Lalagyan>
    );
  }
}